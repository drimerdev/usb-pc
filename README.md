#  USB-PC

An usb-powered [Personal Computer](https://en.wikipedia.org/wiki/Personal_computer)

### Specs
>!NOTE
>Those configurations are from the prototype
- [Z80 Processor]()
- 2 Kilobytes Memory
- 1 MegaByte [ROM]()
- MOB audio chip from commandore64

### Support our project!
<a href="https://www.buymeacoffee.com/mig207709y"><img src="https://img.buymeacoffee.com/button-api/?text=Buy me a coffee&emoji=&slug=mig207709y&button_colour=5F7FFF&font_colour=ffffff&font_family=Cookie&outline_colour=000000&coffee_colour=FFDD00" /></a>
