#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>

// Function to split a string into tokens
std::vector<std::string> split(const std::string &s, char delimiter) {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (std::getline(tokenStream, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;
}

// Function to execute a command
void execute_command(const std::vector<std::string> &tokens) {
    // Convert vector of strings to array of char pointers
    char* argv[tokens.size() + 1];
    for (size_t i = 0; i < tokens.size(); ++i) {
        argv[i] = const_cast<char*>(tokens[i].c_str());
    }
    argv[tokens.size()] = nullptr;

    // Execute the command
    execvp(argv[0], argv);
    // If execvp returns, an error occurred
    std::cerr << "Command not found: " << tokens[0] << std::endl;
    exit(EXIT_FAILURE);
}

int main() {
    while (true) {
        // Print shell prompt
        std::cout <<  "USBSHELL#";
        
        // Read user input
        std::string input;
        std::getline(std::cin, input);

        // Split input into commands based on pipe and command chaining symbols
        std::vector<std::string> command_groups = split(input, '|');
        for (const std::string& command_group : command_groups) {
            // Split each command group into individual commands
            std::vector<std::string> commands = split(command_group, ';');
            for (const std::string& command : commands) {
                // Split each command into tokens
                std::vector<std::string> tokens = split(command, ' ');

                // Check for built-in commands
                if (tokens[0] == "exit") {
                    // Exit the shell
                    exit(EXIT_SUCCESS);
                } else if (tokens[0] == "cd") {
                    // Change directory
                    if (tokens.size() > 1) {
                        chdir(tokens[1].c_str());
                    } else {
                        chdir(getenv("HOME"));
                    }
                    continue;
                }

                // Fork a child process
                pid_t pid = fork();

                if (pid == 0) {
                    // Child process
                    // Execute the command
                    execute_command(tokens);
                } else if (pid < 0) {
                    // Fork failed
                    std::cerr << "Fork failed" << std::endl;
                    exit(EXIT_FAILURE);
                } else {
                    // Parent process
                    // Wait for the child process to terminate
                    int status;
                    waitpid(pid, &status, 0);
                }
            }
        }
    }

    return 0;
}

