BITS 16                     ; Set the assembly mode to 16-bit
ORG 0x7C00                  ; Set the origin address to where BIOS loads the boot sector

start:
    mov ax, 0               ; Set up segments
    mov ds, ax
    mov es, ax
    mov ss, ax              ; Set stack segment
    mov sp, 0x7C00          ; Set stack pointer

    mov si, msg             ; Load message address into SI register
    call print_string       ; Call function to print the message

    jmp $                   ; Infinite loop

print_string:
    mov ah, 0x0E            ; BIOS teletype function
.repeat:
    lodsb                   ; Load next byte of the string into AL
    cmp al, 0               ; Check if it's the null terminator
    je .done                ; If it is, exit the loop
    int 0x10                ; Otherwise, print the character
    jmp .repeat             ; Repeat until null terminator
.done:
    ret

msg db "USB-PC BIOS v1", 0   ; Message to be printed

times 510-($-$$) db 0       ; Fill the rest of the boot sector with zeros
dw 0xAA55                   ; Boot signature

; bios file